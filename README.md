# Week 6 Mini Project
> Oliver Chen (yc557)
## Lambda functionality

This Rust Lambda function project aims to enhance observability by implementing comprehensive logging through the use of the `tracing` crate. Additionally, AWS X-Ray tracing will be integrated to provide detailed insights into function execution. To consolidate logs and traces, the application will seamlessly connect to AWS CloudWatch, enabling efficient monitoring, troubleshooting, and analysis of the Lambda function's performance in the AWS ecosystem.

## Logging Implementation
I reused the lambda funciton used in the Week 2 Mini Project, which calculates the triple of a given number. 

In `Cargo.toml`, I first added the two requirements need for the logging implementation.
```toml
tracing = { version = "0.1", features = ["log"] }
```
```toml
tracing-subscriber = { version = "0.3", default-features = false, features = ["env-filter", "fmt"] }
```
Then in `main.rs`, I added the following code to set up the logger:
```rust
// Set up the logger
let subscriber = FmtSubscriber::builder().with_max_level(Level::INFO).finish();

tracing::subscriber::set_global_default(subscriber).expect("Failed to set up global default logger");

info!("Initializing the service...");
```

## X-Ray Tracing
1. Go to the AWS Management Console. Make sure the following permissions are attached to the role.
![Permissions](/images/permissions.png)
2. Under *Monitoring and operations tools*, enable **AWS X-Ray Active tracing** and **Lambda Insights Enhanced monitoring**.
![Settings](/images/settings.png)

## Testing
A Makefile is included for running commands easier. Specifically:

### Build stage
```bash
make build
```
or
```bash
cargo lambda build --release --arm64
```

### Deploy stage
```bash
make deploy
```
or
```bash
cargo lambda deploy --region us-east-1 --iam-role arn:aws:iam::471112660632:role/ids721
```

### Watch stage
```bash
make watch
```
or 
```bash
cargo lambda watch
```
Here is the result where we can see the logging message:
![Terminal](/images/terminal.png)

### Test stage
We can either use Postman or cURL on the terminal to test the deployed app.
![Postman](/images/postman.png)

```
curl -X POST https://ixxecz9ms5.execute-api.us-east-1.amazonaws.com/test/triple -H "Content-Type: application/json" -d '{"number":4}'
```
Here is the output:

![Terminal-2](/images/terminal-2.png)

## CloudWatch Centralization
Back to the AWS Management Console, we can access **CloudWatch** to check out the traces and details for the lambda function.
![Traces](/images/traces.png)
![Trace Details](/images/trace-detail.png)